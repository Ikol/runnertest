﻿using RunnerTest.Scripts.Config.Booster;
using UnityEngine;

namespace RunnerTest.Scripts.Boosters
{
    public abstract class BoosterGeneric<T> : Booster where T : BoosterPrefabConfig
    {
        public override string AddressablePath => _addressablePath;
        private string _addressablePath;
        private T _boosterConfig;
        
        protected abstract void ApplyBooster(T boosterConfig);
        
        public override void Setup(BoosterPrefabConfig boosterConfig)
        {
            Setup((T) boosterConfig);
        }
        
        private void Setup(T boosterConfig)
        {
            _boosterConfig = boosterConfig;
            _addressablePath = _boosterConfig.AddressablePath;
        }
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                ApplyBooster(_boosterConfig);
                gameObject.SetActive(false);
            }
        }
    }
}