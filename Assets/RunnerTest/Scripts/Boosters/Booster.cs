﻿using RunnerTest.Scripts.Config.Booster;
using RunnerTest.Scripts.Pool;
using UnityEngine;

namespace RunnerTest.Scripts.Boosters
{
    [RequireComponent(typeof(Collider))]
    public abstract class Booster : MonoBehaviour, IPoollable
    {
        public abstract string AddressablePath { get; }
        public GameObject GameObject => gameObject;

        public abstract void Setup(BoosterPrefabConfig boosterConfig);
    }
}