﻿using RunnerTest.Scripts.Config.Booster;
using RunnerTest.Scripts.Core;

namespace RunnerTest.Scripts.Boosters
{
    public class BoosterHealth : BoosterGeneric<BoosterHealthConfig>
    {
        protected override void ApplyBooster(BoosterHealthConfig boosterConfig)
        {
            GameModel.Instance.IncreasePlayerHealth(boosterConfig.HealthBoost);
        }
    }
}