﻿using RunnerTest.Scripts.Config.Booster;
using RunnerTest.Scripts.Core;

namespace RunnerTest.Scripts.Boosters
{
    public class BoosterSpeed : BoosterGeneric<BoosterSpeedConfig>
    {
        protected override void ApplyBooster(BoosterSpeedConfig boosterConfig)
        {
            GameModel.Instance.IncreasePlayerSpeed(boosterConfig.SpeedBoost);
        }
    }
}