﻿using RunnerTest.Scripts.Core;
using UnityEngine;

namespace RunnerTest.Scripts.Blocks
{
    [RequireComponent(typeof(Collider))]
    public class Trap : MonoBehaviour
    {
        [SerializeField] private bool _respawnOnCollision;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                GameModel.Instance.ReducePlayerHealth();
                if (_respawnOnCollision)
                {
                    GameLogic.Instance.RespawnPlayer();
                }
            }
        }
    }
}