﻿using System.Threading.Tasks;
using RunnerTest.Scripts.Boosters;
using RunnerTest.Scripts.Config.Block;
using RunnerTest.Scripts.Config.Booster;
using RunnerTest.Scripts.Pool;
using UnityEngine;
using Random = UnityEngine.Random;

namespace RunnerTest.Scripts.Blocks
{
    public class BlockWithBooster : Block
    {
        [SerializeField] private Transform _boosterParent;

        private ObjectPool _pool;
        private IPoollable _poolableBooster;

        public async Task Setup<T>(T config, ObjectPool pool) where T : BlockPrefabConfigWithBooster
        {
            base.Setup(config);
            _pool = pool;
            
            var randomBoosterIndex = Random.Range(0, config.BoosterConfigs.Length);
            var selectedBoosterConfig = config.BoosterConfigs[randomBoosterIndex];
            
            await CreateBooster(selectedBoosterConfig);
        }

        private async Task CreateBooster<T>(T boosterConfig) where T : BoosterPrefabConfig
        {
            var booster = await _pool.GetPrefabAsync(boosterConfig.AddressablePath);
            booster.transform.parent = _boosterParent;
            booster.transform.localPosition = Vector3.zero;
            
            var boosterPrefab = booster.GetComponent<Booster>();
            boosterPrefab.Setup(boosterConfig);
            _poolableBooster = boosterPrefab;
        }

        private void OnDisable()
        {
            if (_poolableBooster != null)
            {
                _pool.ReturnPrefab(_poolableBooster);
            }
        }
    }
}