﻿using RunnerTest.Scripts.Core;
using UnityEngine;

namespace RunnerTest.Scripts.Blocks
{
    [RequireComponent(typeof(Collider))]
    public class Rotator : MonoBehaviour
    {
        [SerializeField] private float _yRotation;
        
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                GameLogic.Instance.RotatePlayer(_yRotation);
            }
        }
    }
}