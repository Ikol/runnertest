﻿using RunnerTest.Scripts.Config.Block;
using RunnerTest.Scripts.Pool;
using UnityEngine;

namespace RunnerTest.Scripts.Blocks
{
    public class Block : MonoBehaviour, IPoollable
    {
        [SerializeField] private Transform[] _conntectors;
        [SerializeField] private Transform[] _waypoints;

        public string AddressablePath => _blockConfig.AddressablePath;
        public GameObject GameObject => gameObject;
        public string BlockName => _blockConfig.BlockName;
        public bool Countable => _blockConfig.Countable;
        
        public Transform StartConnector() => _conntectors[0];
        public Transform EndConnector() => _conntectors[1];
        private BlockPrefabConfig _blockConfig;
        
        private int _currentWaypointIndex;

        private void OnEnable()
        {
            _currentWaypointIndex = 0;
        }

        public void Setup(BlockPrefabConfig blockConfig)
        {
            _blockConfig = blockConfig;
        }

        public Transform GetNextWaypoint()
        {
            if (_currentWaypointIndex >= _waypoints.Length)
            {
                return null;
            }
            
            return _waypoints[_currentWaypointIndex++];
        }
    }
}