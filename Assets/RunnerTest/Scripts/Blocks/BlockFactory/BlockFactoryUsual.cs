﻿using System.Threading.Tasks;
using RunnerTest.Scripts.Config.Block;
using RunnerTest.Scripts.Pool;

namespace RunnerTest.Scripts.Blocks.BlockFactory
{
    public class BlockFactoryUsual : IBlockFactory
    {
        public async Task<Block> CreateBlock(BlockPrefabConfig config, ObjectPool pool)
        {
            var loadedPrefab = await pool.GetPrefabAsync(config.AddressablePath);
            var component = loadedPrefab.GetComponent<Block>();
            component.Setup(config);
            return component;
        }
    }
}