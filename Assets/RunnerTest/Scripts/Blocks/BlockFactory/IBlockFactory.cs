﻿using System.Threading.Tasks;
using RunnerTest.Scripts.Config.Block;
using RunnerTest.Scripts.Pool;

namespace RunnerTest.Scripts.Blocks.BlockFactory
{
    public interface IBlockFactory
    {
        Task<Block> CreateBlock(BlockPrefabConfig config, ObjectPool pool);
    }
}