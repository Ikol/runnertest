﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RunnerTest.Scripts.Blocks.BlockFactory;
using RunnerTest.Scripts.Config.Block;
using RunnerTest.Scripts.Pool;
using UnityEngine;

namespace RunnerTest.Scripts.Blocks
{
    public class BlockSpawner : MonoBehaviour
    {
        private Dictionary<Type, IBlockFactory> _factories;

        private void Awake()
        {
            _factories = new Dictionary<Type, IBlockFactory>
            {
                {typeof(BlockPrefabConfig), new BlockFactoryUsual()},
                {typeof(BlockPrefabConfigWithBooster), new BlockFactoryWithBooster()}
            };
        }

        private int Counter = 0;

        public async Task<Block> Spawn(ObjectPool objectPool, Transform blocksParent, Transform connectTo)
        {
            var selectedConfig = ConfigStorage.Instance.GetWeightedBlockConfig();
            
            if (_factories.TryGetValue(selectedConfig.GetType(), out var factory) == false)
            {
                Debug.LogError($"Factory for {selectedConfig.GetType()} not found");
                return null;
            }
            
            var loadedPrefab = await factory.CreateBlock(selectedConfig, objectPool);
            var prefabTransform = loadedPrefab.transform;
            prefabTransform.parent = blocksParent;
            prefabTransform.name = $"Block_{Counter++}";
            
            prefabTransform.rotation = connectTo.rotation;
            var startConnector = loadedPrefab.StartConnector();
            var offset = connectTo.position - startConnector.position;
            loadedPrefab.transform.position += offset;

            return loadedPrefab;
        }
    }
}