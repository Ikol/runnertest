﻿using UnityEngine;

namespace RunnerTest.Scripts
{
    public static class Vector3Extension
    {
        public static float GetDistanceToWithoutY(this Vector3 from, Vector3 to)
        {
            var dx = from.x - to.x;
            var dz = from.z - to.z;
            return dx * dx + dz * dz;
        }
    }
}