﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RunnerTest.Scripts.Config;
using RunnerTest.Scripts.Config.Block;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace RunnerTest.Scripts
{
    public class ConfigStorage : MonoBehaviour
    {
        public static ConfigStorage Instance { get; private set; }
        
        [SerializeField] private string _configLabel;
        
        public GameplayConfig GameplayConfig { get; private set; }
        public List<BlockPrefabConfig> BlockConfigs { get; private set; }

        private AsyncOperationHandle<IList<ScriptableObject>> _handle;

        private void Awake()
        {
            Instance = this;
        }

        public async Task LoadConfigs()
        {
            _handle = Addressables.LoadAssetsAsync<ScriptableObject>(_configLabel, null);
            var loadedConfigs = await _handle.Task;
            BlockConfigs = new List<BlockPrefabConfig>();
            
            foreach (var config in loadedConfigs)
            {
                if (config is GameplayConfig gameplayConfig)
                {
                    GameplayConfig = gameplayConfig;
                }
                else if (config is BlockPrefabConfig blockConfig)
                {
                    BlockConfigs.Add(blockConfig);
                }
            }
        }
        
        public BlockPrefabConfig GetWeightedBlockConfig()
        {
            if (BlockConfigs.Count == 0)
            {
                Debug.LogError("No block configs loaded");
                return null;
            }
            
            float totalWeight = 0;
            foreach (var blockConfig in BlockConfigs)
            {
                totalWeight += blockConfig.Weight;
            }
            
            float randomValue = UnityEngine.Random.Range(0, totalWeight);
            
            foreach (var blockConfig in BlockConfigs)
            {
                randomValue -= blockConfig.Weight;
                if (randomValue <= 0)
                {
                    return blockConfig;
                }
            }

            return BlockConfigs[0];
        }
        
        public BlockPrefabConfig GetRandomBlockConfig()
        {
            return BlockConfigs[UnityEngine.Random.Range(0, BlockConfigs.Count)];
        }

        public void OnDestroy()
        {
            Addressables.Release(_handle);
        }
    }
}