﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RunnerTest.Scripts.Config.Booster.MetaEffect;
using UnityEngine;

namespace RunnerTest.Scripts.Core
{
    public enum GameState
    {
        Prepare,
        Gameplay,
        Win,
        Lose
    }
    
    public class GameModel : MonoBehaviour
    {
        public static GameModel Instance { get; private set; }
        
        [SerializeField] private GameLogic _gameLogic;
        
        public Stats Stats { get; private set; }
        public Dictionary<string, int> PassedBlocks { get; private set; } = new Dictionary<string, int>();
        
        private GameState _currentState;
        
        public event Action<GameState> OnStateChange;

        private void Awake()
        {
            Instance = this;
        }

        private async void Start()
        {
            Stats = new Stats();
            await ConfigStorage.Instance.LoadConfigs();
            await RestartGame();
        }

        private void Update()
        {
            if (Stats == null || _currentState != GameState.Gameplay)
            {
                return;
            }
            
            Stats.UpdateBoosterDurations();
        }

        public void OnActionButtonClick()
        {
            switch (_currentState)
            {
                case GameState.Prepare:
                    SetBaseStats();
                    SetState(GameState.Gameplay);
                    break;
                case GameState.Gameplay:
                    Jump();
                    break;
            }
        }

        public async Task RestartGame()
        {
            SetState(GameState.Prepare);
            await _gameLogic.Prepare();
        }

        public void ContinueAfterAd()
        {
            Stats.Health = ConfigStorage.Instance.GameplayConfig.ExtraHealth;
            AddEffect(ConfigStorage.Instance.GameplayConfig.DebuffEffect);
            SetState(GameState.Gameplay);
        }

        public void AddEffect(Effect effect)
        {
            Stats.AddEffect(effect);
        }

        private void SetState(GameState newState)
        {
            _currentState = newState;
            OnStateChange?.Invoke(_currentState);
            if (_currentState == GameState.Prepare)
            {
                PassedBlocks.Clear();
            }
        }

        private void SetBaseStats()
        {
            Stats.Speed = ConfigStorage.Instance.GameplayConfig.BaseSpeed;
            Stats.Health = ConfigStorage.Instance.GameplayConfig.BaseHealth;
        }

        private void Jump()
        {
            _gameLogic.Jump();
        }
        
        public void IncreasePlayerHealth(float value)
        {
            Stats.Health += value;
        }

        public void IncreasePlayerSpeed(float value)
        {
            Stats.Speed += value;
        }
        
        public void ReducePlayerHealth()
        {
            if (Stats.Invulnerable)
            {
                return;
            }
            
            Stats.Health--;
            if (Stats.Health <= 0)
            {
                SetState(GameState.Lose);
            }
        }
        
        public void FinishGame()
        {
            SetState(GameState.Win);
        }

        public void AddBlockToScore(string blockId)
        {
            if (PassedBlocks.ContainsKey(blockId))
            {
                PassedBlocks[blockId]++;
            }
            else
            {
                PassedBlocks.Add(blockId, 1);
            }
        }
    }
}