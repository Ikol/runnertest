﻿using System;
using System.Collections.Generic;
using RunnerTest.Scripts.Config.Booster.MetaEffect;
using UnityEngine;

namespace RunnerTest.Scripts.Core
{
    public class ActiveEffect
    {
        public Effect Effect;
        public float Duration;
    }
    
    public class Stats
    {
        public event Action<float, float> OnSpeedChanged;
        public event Action<float, float> OnHealthChanged;
        public event Action<bool, bool> OnInvulnerableChanged;
        
        private List<ActiveEffect> _activeEffects = new List<ActiveEffect>();
        private List<ActiveEffect> _expiredEffects = new List<ActiveEffect>();
        
        private float _speed;
        private float _extraSpeed;
        private float _health;
        private bool _invulnerable;
        
        public float Speed
        {
            get => _speed + ExtraSpeed;
            set
            {
                OnSpeedChanged?.Invoke(_speed, value);
                _speed = value;
            }
        }

        public float ExtraSpeed
        {
            get => _extraSpeed;
            set
            {
                OnSpeedChanged?.Invoke(Speed, _speed + value);
                _extraSpeed = value;
            }
        }

        public float Health
        {
            get => _health;
            set
            {
                OnHealthChanged?.Invoke(_health, value);
                _health = value;
            }
        }
        
        public bool Invulnerable
        {
            get => _invulnerable;
            set
            {
                OnInvulnerableChanged?.Invoke(_invulnerable, value);
                _invulnerable = value;
            }
        }
        
        public void AddEffect(Effect effect)
        {
            var foundEffect = _activeEffects.Find(x => x.Effect == effect);
            if (foundEffect != null)
            {
                foundEffect.Duration += effect.DurationSeconds;
                return;
            }
            
            _activeEffects.Add(new ActiveEffect{Effect = effect, Duration = effect.DurationSeconds});
            effect.Apply(this);
        }
        
        public void RemoveEffect(Effect effect)
        {
            var foundEffect = _activeEffects.Find(x => x.Effect == effect);
            if (foundEffect == null)
            {
                return;
            }
            
            effect.Remove(this);
            _activeEffects.Remove(foundEffect);
        }

        public void UpdateBoosterDurations()
        {
            if (_activeEffects.Count == 0)
            {
                return;
            }

            foreach (var activeEffect in _activeEffects)
            {
                activeEffect.Duration -= Time.deltaTime;
                activeEffect.Effect.Tick(this, activeEffect.Duration);
                
                if (activeEffect.Duration <= 0)
                {
                    _expiredEffects.Add(activeEffect);
                }
            }

            RemoveExpiredEffects();
        }
        
        private void RemoveExpiredEffects()
        {
            foreach (ActiveEffect key in _expiredEffects)
            {
                RemoveEffect(key.Effect);
            }
            
            _expiredEffects.Clear();
        }
    }
}