﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RunnerTest.Scripts.Blocks;
using RunnerTest.Scripts.Pool;
using UnityEngine;

namespace RunnerTest.Scripts.Core
{
    public class GameLogic : MonoBehaviour
    {
        public static GameLogic Instance { get; private set; }

        [SerializeField] private ObjectPool _objectPool;
        [SerializeField] private PlayerLoader _playerLoader;
        [SerializeField] private BlockSpawner _blockSpawner;
        [SerializeField] private Transform _startPoint;
        [SerializeField] private Transform _platformParent;
        [SerializeField] private TargetFollower _cameraFollower;
        [SerializeField] private int _maxBlocks = 3;
        
        private PlayerController _playerController;
        private Transform _nextWaypoint;
        
        private Transform _nextSpawnPoint;
        private Queue<Block> _spawnedBlocks;
        private Block _recycledBlock;
        private int _blockCounter;

        private void Awake()
        {
            Instance = this;
        }

        private void OnDestroy()
        {
            _playerLoader.UnloadPlayer();
        }

        public async Task Prepare()
        {
            CleanAll();
            await SpawnFirstBlocks();
            _nextWaypoint = RequestNextWaypoint();

            if (_playerController == null)
            {
                _playerController = await _playerLoader.LoadPlayer(ConfigStorage.Instance.GameplayConfig.PlayerPrefabAddressablePath);
                _cameraFollower.Setup(_playerController.transform);
            }
            
            RespawnPlayer();
        }
        
        private void CleanAll()
        {
            _blockCounter = 0;
            if (_spawnedBlocks != null)
            {
                foreach (var spawnedBlock in _spawnedBlocks)
                {
                    _objectPool.ReturnPrefab(spawnedBlock);
                }
            }

            if (_recycledBlock != null)
            {
                _objectPool.ReturnPrefab(_recycledBlock);
            }

            _nextSpawnPoint = _startPoint;
            _nextWaypoint = null;
            _recycledBlock = null;
            _spawnedBlocks = null;
        }

        public void RespawnPlayer()
        {
            _playerController.transform.position = _nextWaypoint.position;
            _playerController.SetRotation(_nextWaypoint.rotation);
        }

        private async Task SpawnFirstBlocks()
        {
            _spawnedBlocks = new Queue<Block>(_maxBlocks);
            _nextSpawnPoint = _startPoint;
            for (int i = 0; i < _maxBlocks; i++)
            {
                await SpawnBlock();
            }
        }
        
        private async Task SpawnBlock()
        {
            if (_blockCounter >= ConfigStorage.Instance.GameplayConfig.TotalBlocks)
            {
                return;
            }
            
            var block = await _blockSpawner.Spawn(_objectPool, _platformParent, _nextSpawnPoint);
            
            _spawnedBlocks.Enqueue(block);
            _nextSpawnPoint = block.EndConnector();
            _blockCounter++;
        }
        
        private void RecycleBlock()
        {
            if (_recycledBlock != null)
            {
                _objectPool.ReturnPrefab(_recycledBlock);
            }
            _recycledBlock = _spawnedBlocks.Dequeue();
            if (_recycledBlock.Countable)
            {
                GameModel.Instance.AddBlockToScore(_recycledBlock.BlockName);
            }
        }

        public void RotatePlayer(float yRotationAngle)
        {
            _playerController.AddRotation(yRotationAngle);
        }
        
        public Transform RequestNextWaypoint()
        {
            _nextWaypoint = GetNextWaypoint();
            return _nextWaypoint;
        }
        
        private Transform GetNextWaypoint()
        {
            if (_spawnedBlocks.Count == 0)
            {
                return null;
            }
            var waypoint = _spawnedBlocks.Peek().GetNextWaypoint();
            if (waypoint == null)
            {
                RecycleBlock();
                
                if (_spawnedBlocks.Count == 0)
                {
                    GameModel.Instance.FinishGame();
                    return null;
                }
                
                waypoint = _spawnedBlocks.Peek().GetNextWaypoint();
                SpawnBlock();
            }
            return waypoint;
        }

        public void Jump()
        {
            _playerController.Jump();
        }
    }
}