﻿using RunnerTest.Scripts.Core;
using RunnerTest.Scripts.UI.Windows;
using RunnerTest.Scripts.UI.Windows.Core;
using UnityEngine;
using UnityEngine.UI;

namespace RunnerTest.Scripts.UI
{
    public class GameplayUI : MonoBehaviour
    {
        [SerializeField] private WindowsController _windowsController;
        [SerializeField] private Button _mainControlButton;
        [SerializeField] private GameObject _preparePanel;
        [SerializeField] private GameplayPanel _gameplayPanel;

        private void OnEnable()
        {
            GameModel.Instance.OnStateChange += OnGameStateChanged;
            _mainControlButton.onClick.AddListener(OnMainControlButtonClick);
        }

        private void OnDisable()
        {
            GameModel.Instance.OnStateChange -= OnGameStateChanged;
            _mainControlButton.onClick.RemoveListener(OnMainControlButtonClick);
        }
        
        private void OnMainControlButtonClick()
        {
            GameModel.Instance.OnActionButtonClick();
        }
        
        private void OnGameStateChanged(GameState state)
        {
            switch (state)
            {
                case GameState.Prepare:
                    _mainControlButton.gameObject.SetActive(true);
                    _preparePanel.SetActive(true);
                    _gameplayPanel.gameObject.SetActive(false);
                    break;
                case GameState.Gameplay:
                    _mainControlButton.gameObject.SetActive(true);
                    _preparePanel.SetActive(false);
                    _gameplayPanel.gameObject.SetActive(true);
                    break;
                case GameState.Win:
                    _mainControlButton.gameObject.SetActive(false);
                    _windowsController.ShowWindow<WindowWin, WindowWinData>(new WindowWinData
                    {
                        BlocksPassed = GameModel.Instance.PassedBlocks
                    });
                    break;
                case GameState.Lose:
                    _mainControlButton.gameObject.SetActive(false);
                    _windowsController.ShowWindow<WindowLose, WindowLoseData>(new WindowLoseData
                    {
                        BlocksPassed = GameModel.Instance.PassedBlocks
                    });
                    break;
            }
        }
    }
}