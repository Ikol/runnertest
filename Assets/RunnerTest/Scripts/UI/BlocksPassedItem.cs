﻿using TMPro;
using UnityEngine;

namespace RunnerTest.Scripts.UI
{
    public class BlocksPassedItem : MonoBehaviour
    {
        [SerializeField] private TMP_Text _blockName;
        [SerializeField] private TMP_Text _blockCount;

        public void Setup(string blockName, int blockCount)
        {
            _blockName.text = blockName;
            _blockCount.text = blockCount.ToString();
        }
    }
}