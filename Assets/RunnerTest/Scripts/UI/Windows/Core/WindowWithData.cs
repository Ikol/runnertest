﻿namespace RunnerTest.Scripts.UI.Windows.Core
{
    public class WindowWithData<T> : Window where T : WindowData
    {
        public virtual void Show(T data)
        {
            base.Show();
        }
    }
}