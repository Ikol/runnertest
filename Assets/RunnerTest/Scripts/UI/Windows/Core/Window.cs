﻿using UnityEngine;

namespace RunnerTest.Scripts.UI.Windows.Core
{
    public class Window : MonoBehaviour
    {
        protected WindowsController WindowsController { get; private set; }
        
        public virtual void Init(WindowsController windowsController)
        {
            WindowsController = windowsController;
        }
        
        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
            WindowsController.CloseWindow(this);
        }
    }
}