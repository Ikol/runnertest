﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace RunnerTest.Scripts.UI.Windows.Core
{
    public class WindowsController : MonoBehaviour
    {
        [SerializeField] private Transform _windowParent;
        
        private List<Window> _windows = new List<Window>();
        
        public async Task<T> ShowWindow<T>() where T : Window
        {
            var prefab = await Addressables.InstantiateAsync($"UI/Windows/{typeof(T).Name}.prefab", _windowParent).Task;
            var window = prefab.GetComponent<T>();
            window.Init(this);
            window.Show();
            _windows.Add(window);
            return window;
        }
        
        public async Task<TWindow> ShowWindow<TWindow, TData>(TData data) where TWindow : WindowWithData<TData> where TData : WindowData
        {
            var prefab = await Addressables.InstantiateAsync($"UI/Windows/{typeof(TWindow).Name}.prefab", _windowParent).Task;
            var window = prefab.GetComponent<TWindow>();
            window.Init(this);
            window.Show(data);
            _windows.Add(window);
            return window;
        }

        public void CloseWindow<T>(T windowInstance) where T : Window
        {
            var window = _windows.Find(w => w == windowInstance);
            if (window == null)
            {
                return;
            }
            
            _windows.Remove(window);
            window.Hide();
            Addressables.ReleaseInstance(window.gameObject);
        }
        
    }
}