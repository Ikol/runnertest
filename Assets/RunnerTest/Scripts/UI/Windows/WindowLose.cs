﻿using System.Collections.Generic;
using RunnerTest.Scripts.Core;
using RunnerTest.Scripts.UI.Windows.Core;
using UnityEngine;
using UnityEngine.UI;

namespace RunnerTest.Scripts.UI.Windows
{
    public class WindowLoseData : WindowData
    {
        public Dictionary<string, int> BlocksPassed;
    }
    
    public class WindowLose : WindowWithData<WindowLoseData>
    {
        [SerializeField] private BlocksPassedList _blocksPassedList;
        [SerializeField] private Button _restartButton;
        [SerializeField] private Button _watchAdButton;
        
        private void OnEnable()
        {
            _restartButton.onClick.AddListener(OnContinueButtonClick);
            _watchAdButton.onClick.AddListener(OnWatchAdButtonClick);
        }

        private void OnDisable()
        {
            _restartButton.onClick.RemoveListener(OnContinueButtonClick);
            _watchAdButton.onClick.RemoveListener(OnWatchAdButtonClick);
        }

        public override void Show(WindowLoseData data)
        {
            base.Show(data);
            _blocksPassedList.Setup(data.BlocksPassed);
        }
        
        private void OnContinueButtonClick()
        {
            Hide();
            GameModel.Instance.RestartGame();
        }
        
        private void OnWatchAdButtonClick()
        {
            Hide();
            GameModel.Instance.ContinueAfterAd();
        }
    }
}