﻿using System.Collections.Generic;
using RunnerTest.Scripts.Core;
using RunnerTest.Scripts.UI.Windows.Core;
using UnityEngine;
using UnityEngine.UI;

namespace RunnerTest.Scripts.UI.Windows
{
    public class WindowWinData : WindowData
    {
        public Dictionary<string, int> BlocksPassed;
    }
    
    public class WindowWin : WindowWithData<WindowWinData>
    {
        [SerializeField] private BlocksPassedList _blocksPassedList;
        [SerializeField] private Button _continueButton;

        private void OnEnable()
        {
            _continueButton.onClick.AddListener(OnContinueButtonClick);
        }

        private void OnDisable()
        {
            _continueButton.onClick.RemoveListener(OnContinueButtonClick);
        }

        public override void Show(WindowWinData data)
        {
            base.Show(data);
            _blocksPassedList.Setup(data.BlocksPassed);
        }
        
        private void OnContinueButtonClick()
        {
            Hide();
            GameModel.Instance.RestartGame();
        }
    }
}