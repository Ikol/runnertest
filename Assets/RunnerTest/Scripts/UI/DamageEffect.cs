﻿using System.Collections;
using UnityEngine;

namespace RunnerTest.Scripts.UI
{
    public class DamageEffect : MonoBehaviour
    {
        [SerializeField] private CanvasGroup _canvasGroup;
        [SerializeField] private int _blinkCount;
        [SerializeField] private float _blinkDuration;
        [SerializeField] private float _blinkInterval;

        private void Start()
        {
            _canvasGroup.alpha = 0;
        }

        public void Show()
        {
            StartCoroutine(Blink());
        }
        
        private IEnumerator Blink()
        {
            for (var i = 0; i < _blinkCount; i++)
            {
                _canvasGroup.alpha = 1;
                yield return new WaitForSeconds(_blinkDuration);
                _canvasGroup.alpha = 0;
                yield return new WaitForSeconds(_blinkInterval);
            }
        }
    }
}