﻿using RunnerTest.Scripts.Core;
using TMPro;
using UnityEngine;

namespace RunnerTest.Scripts.UI
{
    public class GameplayPanel : MonoBehaviour
    {
        [SerializeField] private DamageEffect _damageEffect;
        [SerializeField] private InvulnerabilityEffect _invulnerabilityEffect;
        [SerializeField] private TMP_Text _speedValue;
        [SerializeField] private TMP_Text _healthValue;

        private void OnEnable()
        {
            GameModel.Instance.OnStateChange += OnGameStateChanged;
            GameModel.Instance.Stats.OnSpeedChanged += SetSpeed;
            GameModel.Instance.Stats.OnHealthChanged += SetHealth;
            GameModel.Instance.Stats.OnInvulnerableChanged += SetInvulnerable;
            
            ResetPanel();
        }

        private void OnDisable()
        {
            GameModel.Instance.OnStateChange -= OnGameStateChanged;
            GameModel.Instance.Stats.OnSpeedChanged -= SetSpeed;
            GameModel.Instance.Stats.OnHealthChanged -= SetHealth;
            GameModel.Instance.Stats.OnInvulnerableChanged -= SetInvulnerable;
        }
        
        private void ResetPanel()
        {
            SetSpeed(0, GameModel.Instance.Stats.Speed);
            SetHealth(0, GameModel.Instance.Stats.Health);
            _invulnerabilityEffect.SetActive(false);
        }
        
        private void OnGameStateChanged(GameState state)
        {
            switch (state)
            {
                case GameState.Gameplay:
                    SetSpeed(0, GameModel.Instance.Stats.Speed);
                    SetHealth(0, GameModel.Instance.Stats.Health);
                    break;
            }
        }
        
        private void SetSpeed(float oldValue, float newValue)
        {
            _speedValue.text = newValue.ToString("F2");
        }
        
        private void SetHealth(float oldValue, float newValue)
        {
            _healthValue.text = newValue.ToString();
            if (oldValue > newValue)
            {
                _damageEffect.Show();
            }
        }
        
        private void SetInvulnerable(bool oldValue, bool newValue)
        {
            _invulnerabilityEffect.SetActive(newValue);
        }
    }
}