﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;

namespace RunnerTest.Scripts.UI
{
    public class BlocksPassedList : MonoBehaviour
    {
        [SerializeField] private ScrollRect _scroll;
        [SerializeField] private string _templatePath;
        
        private List<BlocksPassedItem> _items = new List<BlocksPassedItem>();
        
        public async Task Setup(Dictionary<string, int> blocksPassed)
        {
            foreach (var block in blocksPassed)
            {
                var itemPrefab = await Addressables.LoadAssetAsync<GameObject>(_templatePath).Task;
                var item = Instantiate(itemPrefab, _scroll.content).GetComponent<BlocksPassedItem>();
                item.Setup(block.Key, block.Value);
                _items.Add(item);
            }
        }

        private void OnDestroy()
        {
            foreach (var item in _items)
            {
                Addressables.ReleaseInstance(item.gameObject);
            }
        }
    }
}