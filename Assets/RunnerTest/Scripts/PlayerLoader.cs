﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace RunnerTest.Scripts
{
    public class PlayerLoader : MonoBehaviour
    {
        private AsyncOperationHandle<GameObject> _handle;
        
        public async Task<PlayerController> LoadPlayer(string playerAddressablePath)
        {
            _handle = Addressables.LoadAssetAsync<GameObject>(playerAddressablePath);
            var playerPrefab = await _handle.Task;
            var player = Instantiate(playerPrefab).GetComponent<PlayerController>();
            return player;
        }
        
        public void UnloadPlayer()
        {
            Addressables.Release(_handle);
        }
    }
}