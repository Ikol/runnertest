﻿using UnityEngine;

namespace RunnerTest.Scripts
{
    public class TargetFollower : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        [SerializeField] private Vector3 _offset;
        [SerializeField] private float _smoothSpeed;
        
        public void Setup(Transform target)
        {
            _target = target;
        }

        private void LateUpdate()
        {
            if (_target == null)
            {
                return;
            }
            
            var newPosition = _target.position + _target.TransformDirection(_offset);
            transform.position = Vector3.Lerp(transform.position, newPosition, _smoothSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, _target.rotation, _smoothSpeed);
        }
    }
}