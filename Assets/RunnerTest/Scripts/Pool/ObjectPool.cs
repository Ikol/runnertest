﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace RunnerTest.Scripts.Pool
{
    public class ObjectPool : MonoBehaviour
    {
        private Dictionary<string, Queue<GameObject>> _pool = new Dictionary<string, Queue<GameObject>>();
        
        public async Task<GameObject> GetPrefabAsync(string prefabId)
        {
            if (!_pool.ContainsKey(prefabId))
            {
                _pool[prefabId] = new Queue<GameObject>();
            }
            
            if (_pool[prefabId].Count == 0)
            {
                return await LoadObjectAsync(prefabId);
            }
            
            var existingPrefab = _pool[prefabId].Dequeue();
            existingPrefab.SetActive(true);
            return existingPrefab;
        }
        
        private async Task<GameObject> LoadObjectAsync(string prefabId)
        {
            return await Addressables.InstantiateAsync(prefabId).Task;
        }
        
        public void ReturnPrefab(IPoollable prefab)
        {
            prefab.GameObject.SetActive(false);
            _pool[prefab.AddressablePath].Enqueue(prefab.GameObject);
        }
        
        private void OnDestroy()
        {
            foreach (var prefabQueue in _pool)
            {
                foreach (var prefab in prefabQueue.Value)
                {
                    if (prefab != null)
                    {
                        Addressables.ReleaseInstance(prefab);
                    }
                }
            }
        }
    }
}