﻿using UnityEngine;

namespace RunnerTest.Scripts.Pool
{
    public interface IPoollable
    {
        public string AddressablePath { get; }
        public GameObject GameObject { get; }
    }
}