﻿using RunnerTest.Scripts.Core;
using UnityEngine;

namespace RunnerTest.Scripts.Config.Booster.MetaEffect
{
    [CreateAssetMenu(fileName = "SpeedDecreaseEffect", menuName = "RunnerTest/SpeedDecreaseEffect")]
    public class SpeedDecreaseEffect : Effect
    {
        [Range(0, 1)]
        [SerializeField] private float _speedDecrease;

        private float _appliedExtraSpeed;
        public override void Apply(Stats stats)
        {
            stats.ExtraSpeed = stats.Speed * _speedDecrease * -1;
            _appliedExtraSpeed = stats.ExtraSpeed;
        }

        public override void Tick(Stats stats, float timeLeft)
        {
            if (DurationSeconds == 0)
            {
                return;
            }
            
            stats.ExtraSpeed = Mathf.Lerp(_appliedExtraSpeed, 0, (DurationSeconds - timeLeft) / DurationSeconds);
        }

        public override void Remove(Stats stats)
        {
            stats.ExtraSpeed = 0;
        }
    }
}