﻿using RunnerTest.Scripts.Core;
using UnityEngine;

namespace RunnerTest.Scripts.Config.Booster.MetaEffect
{
    public abstract class Effect : ScriptableObject
    {
        public float DurationSeconds;
        
        public abstract void Apply(Stats stats);
        public abstract void Tick(Stats stats, float timeLeft);
        public abstract void Remove(Stats stats);
        
        public override int GetHashCode()
        {
            return name.GetHashCode() ^ GetType().GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Effect other = (Effect)obj;
            return name == other.name && GetType() == other.GetType();
        }
    }
}