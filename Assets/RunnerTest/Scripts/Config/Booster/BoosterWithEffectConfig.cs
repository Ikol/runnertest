﻿using RunnerTest.Scripts.Config.Booster.MetaEffect;

namespace RunnerTest.Scripts.Config.Booster
{
    public class BoosterWithEffectConfig : BoosterPrefabConfig
    {
        public Effect Effect;
    }
}