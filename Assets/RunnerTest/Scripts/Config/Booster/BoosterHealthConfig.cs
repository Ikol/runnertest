﻿using UnityEngine;

namespace RunnerTest.Scripts.Config.Booster
{
    [CreateAssetMenu(fileName = "BoosterHealthConfig", menuName = "RunnerTest/BoosterHealthConfig")]
    public class BoosterHealthConfig : BoosterPrefabConfig
    {
        public float HealthBoost;
    }
}