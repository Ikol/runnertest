﻿using UnityEngine;

namespace RunnerTest.Scripts.Config.Booster
{
    [CreateAssetMenu(fileName = "BoosterSpeedConfig", menuName = "RunnerTest/BoosterSpeedConfig")]
    public class BoosterSpeedConfig : BoosterPrefabConfig
    {
        public float SpeedBoost;
    }
}