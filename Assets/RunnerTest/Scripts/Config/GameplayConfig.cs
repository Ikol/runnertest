﻿using RunnerTest.Scripts.Config.Booster.MetaEffect;
using UnityEngine;

namespace RunnerTest.Scripts.Config
{
    [CreateAssetMenu(fileName = "GameplayConfig", menuName = "RunnerTest/GameplayConfig")]
    public class GameplayConfig : ScriptableObject
    {
        [Header("Game Level")]
        public int TotalBlocks;
        public float BaseSpeed;
        public float BaseHealth;
        
        [Header("Ad respawn")]
        public SpeedDecreaseEffect DebuffEffect;
        public float ExtraHealth;
        
        [Header("Character")]
        public string PlayerPrefabAddressablePath;
    }
}