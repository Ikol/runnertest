﻿using UnityEngine;

namespace RunnerTest.Scripts.Config.Block
{
    [CreateAssetMenu(fileName = "BlockPrefabConfig", menuName = "RunnerTest/BlockConfig")]
    public class BlockPrefabConfig : LoadablePrefabConfig
    {
        public string BlockName;
        public int Weight;
        public bool Countable;
    }
}