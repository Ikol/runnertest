﻿using RunnerTest.Scripts.Config.Booster;
using UnityEngine;

namespace RunnerTest.Scripts.Config.Block
{
    [CreateAssetMenu(fileName = "BlockPrefabConfigWithBooster", menuName = "RunnerTest/BlockWithBoosterConfig")]
    public class BlockPrefabConfigWithBooster : BlockPrefabConfig
    {
        public BoosterPrefabConfig[] BoosterConfigs;
    }
}