﻿using UnityEngine;

namespace RunnerTest.Scripts.Config
{
    public class LoadablePrefabConfig : ScriptableObject
    {
        public string AddressablePath;
    }
}