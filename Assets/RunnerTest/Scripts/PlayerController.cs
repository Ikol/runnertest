﻿using RunnerTest.Scripts.Core;
using UnityEngine;

namespace RunnerTest.Scripts
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Animator _animator;
        [SerializeField] private Rigidbody _rigidBody;
        [SerializeField] private float _jumpCooldown;
        
        private Quaternion _targetRotation;
        private Transform _targetWaipoint;
        private float _speed;
        private bool _running;
        private bool _isGrounded;
        private bool _doubleJumpPossible;
        private float _jumpTimer;
        
        private static readonly int JumpTrigger = Animator.StringToHash("Jump");
        private static readonly int RunSpeedFloat = Animator.StringToHash("RunSpeed");
        private static readonly int IsGroundedBool = Animator.StringToHash("IsGrounded");

        private void OnEnable()
        {
            GameModel.Instance.OnStateChange += OnGameStateChanged;
            GameModel.Instance.Stats.OnSpeedChanged += ChangeSpeed;
        }

        private void OnDisable()
        {
            GameModel.Instance.OnStateChange -= OnGameStateChanged;
            GameModel.Instance.Stats.OnSpeedChanged -= ChangeSpeed;
        }

        private void Start()
        {
            _speed = GameModel.Instance.Stats.Speed;
        }

        private void Update()
        {
            if (!_running)
            {
                return;
            }
            
            _isGrounded = IsGrounded();
            _animator.SetBool(IsGroundedBool, _isGrounded);

            if (_jumpTimer > 0)
            {
                _jumpTimer -= Time.deltaTime;
            }

            if (_isGrounded)
            {
                _doubleJumpPossible = true;
            }
            
            if (_rigidBody.position.GetDistanceToWithoutY(_targetWaipoint.position) < .1f)
            {
                _targetWaipoint = GameLogic.Instance.RequestNextWaypoint();
            }
            
            RotatePlayer();
        }

        private void FixedUpdate()
        {
            if (_targetWaipoint == null || !_running)
            {
                return;
            }
            
            MoveToNextWaypoint();
        }

        public void Jump()
        {
            if (_jumpTimer > 0)
            {
                return;
            }
            
            if (!_isGrounded)
            {
                if (!_doubleJumpPossible)
                {
                    return;
                }
                _doubleJumpPossible = false;
            }

            _jumpTimer = _jumpCooldown;
            _rigidBody.AddForce(Vector3.up * 4, ForceMode.Impulse);
            _animator.SetTrigger(JumpTrigger);
        }

        private void OnGameStateChanged(GameState state)
        {
            switch (state)
            {
                case GameState.Gameplay:
                    _targetWaipoint = GameLogic.Instance.RequestNextWaypoint();
                    _running = true;
                    break;
                case GameState.Prepare:
                case GameState.Win:
                case GameState.Lose:
                    _rigidBody.velocity = Vector3.zero;
                    _running = false;
                    _targetWaipoint = null;
                    if (!_isGrounded)
                    {
                        _isGrounded = true;
                        _animator.SetBool(IsGroundedBool, _isGrounded);
                    }
                    _animator.SetFloat(RunSpeedFloat, 0);
                    break;
            }
        }

        private void ChangeSpeed(float oldValue, float newValue)
        {
            _speed = newValue;
            _animator.SetFloat(RunSpeedFloat, _speed * .1f);
        }

        public void SetRotation(Quaternion rotation)
        {
            _targetRotation = rotation;
            _rigidBody.rotation = rotation;
        }
        
        public void AddRotation(float yRotationAngle)
        {
            _targetRotation = _rigidBody.rotation * Quaternion.Euler(0, yRotationAngle, 0);
        }
        
        private void RotatePlayer()
        {
            _rigidBody.MoveRotation(Quaternion.Lerp(_rigidBody.rotation, _targetRotation, Time.fixedDeltaTime * 6f));
        }
        
        private void MoveToNextWaypoint()
        {
            var direction = _targetWaipoint.position - _rigidBody.position;
            direction.y = 0;
            direction.Normalize();
            _rigidBody.velocity = new Vector3(direction.x * _speed, _rigidBody.velocity.y, direction.z * _speed);
        }
        
        private bool IsGrounded()
        {
            return Physics.Raycast(transform.position, Vector3.down, 0.1f);
        }
    }
}